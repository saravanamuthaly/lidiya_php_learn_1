<?php

$text = "apple is a fruit";
var_dump( $text );

$word = "     apple  ";
var_dump($word);
$trimmed_word = trim($word);
var_dump($trimmed_word);

var_dump(ltrim($word));
var_dump(rtrim($word));

var_dump(strtoupper(trim($word)));
var_dump(strtolower(trim($word)));
var_dump(ucfirst(trim($word)));
var_dump(ucwords($text));

?>
<p>$text = <?php echo $text; ?></p>
<p>strlen($text) = <?php echo strlen( $text ); ?></p>
<p>str_word_count($text) = <?php echo str_word_count( $text ); ?></p>
<p>strrev($text) = <?php echo strrev( $text ); ?></p>
<p>strpos($text, 'is') = <?php echo strpos( $text, "is" ); ?></p>
<p>str_replace("is", "are", $text) = <?php echo str_replace( "is", "are", $text ); ?></p>

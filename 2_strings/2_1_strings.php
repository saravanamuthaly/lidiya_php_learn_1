<?php

$name = "Dave";
$age = 21;

$text1 = "Your name is $name and you are $age years old.";  // Your name is Dave
$text2 = 'Your "name" is $name';

?>

<p><?php echo $text1 ?></p>

<p><?php echo $text2 ?></p>


<?php

/*
 * " => \"
 * ' => \'
 * [enter] => \n
 * [tab] => \t
 * \ => \\
 */

$text = "Dave said, \"You are a genius.\""; // Dave said, "You are a genius."

?>

<p><?php echo $text; ?></p>

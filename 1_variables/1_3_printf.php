<?php

$name = "Dave";
$age = 12;
$color = "pink";

$statement = "My name is " . $name . ", and I am " . $age . " years old. I like " . $color . " color.";

$statement2 = sprintf( "statement2: My name is %s, and I am %d years old. I like %s color.", $name, $age, $color );

// My name is David, and I am 12 years old. I like red color.

?>



<p>
    <?php printf( "My name is %s, and I am %d years old. I like %s color.", $name, $age, $color ); ?>
</p>
<p><?php echo $statement; ?></p>
<p>
    My name is <?php echo $name ?>, and I am <?php echo $age ?> years old. I like <?php echo $color ?> color.
</p>

<p><?php echo $statement2 ?></p>

<?php

class Book
{
    /*
     * attributes
     */
    public string $title;


    /*
     * functions
     */

    public function hi()
    {
        echo "<p>hi, I am " . $this->title . "</p>";
    }
}


$my_book = new Book();
$my_book->title = "This is a book";
$my_book->hi();

$book2 = new Book();
$book2->title = "Book 2";
$book2->hi();
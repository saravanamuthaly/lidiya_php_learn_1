<?php


class Student
{
    public string $name;
    public int $grade;

    public array $subjects;

    public function __construct( $name, $grade )
    {
        $this->name = $name;
        $this->grade = $grade;
        $this->subjects = [];
    }

    public function addSubject($subject){
        $this->subjects[] = $subject;
    }

    public function removeSubject($subject){
        $foundKey = array_search($subject, $this->subjects);
        if(!$foundKey) return;

        array_splice($this->subjects, $foundKey, 1);
    }

    public function setSubjects($subjectsArray){
        $this->subjects = $subjectsArray;
    }


}


$s1 = new Student("Dave", 11);

$s1->addSubject("ICT");
$s1->addSubject("Science");
$s1->addSubject("Math");
$s1->addSubject("History");
$s1->addSubject("Tamil");

var_dump($s1->subjects);

$s1->removeSubject("Math");
$s1->removeSubject("Health Science");

var_dump($s1->subjects);


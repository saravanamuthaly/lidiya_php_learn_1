<?php

class Book
{
    public string $title;
    private float $price;
    private int $bookCount;


    public function __construct( $title, $price = 0 , $bookCount = 0 )
    {
        $this->title = $title;
        $this->setPrice($price);
        $this->setBookCount($bookCount);
        echo "<pre>book is being constructed</pre>";
    }

    public function setPrice( $price )
    {
        if ( $price < 0 ) throw new Exception( "Invalid price" );

        $this->price = $price;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getBookCount()
    {
        return $this->bookCount;
    }

    public function setBookCount( int $bookCount )
    {
        if ( $bookCount < 0 ) {
            throw new Exception( "Invalid book count" );
        }
        $this->bookCount = $bookCount;
    }

    public function getTotalPrice()
    {
        return $this->price * $this->bookCount;
    }

}

$book1 = new Book( "Ambulimama Stories", 450, 25 );
$book2 = new Book( "Vikaramathithan Stories" );

var_dump( $book1 );
var_dump( $book2->getBookCount() );

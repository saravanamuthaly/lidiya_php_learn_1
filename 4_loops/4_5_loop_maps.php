<?php

$employees = [
    [
        "name" => "Dave",
        "department" => "ICT",
        "salary" => 35000.0,
        "tax" => 1500.0,
    ],
    [
        "name" => "Kumar",
        "department" => "Accounting",
        "salary" => 56000.0,
        "tax" => 4200.0,
    ],
];


$new_employees = [];

foreach ( $employees as $employee ) {
    $employee[ "net_income" ] = $employee["salary"] - $employee["tax"];
    $new_employees[] = $employee;
}

echo json_encode( $new_employees );



//
//foreach ( $employees as $employee ) {
//    $net_income = $employee["salary"] - $employee["tax"];
//    printf("<p>%s works in %s and his net salary is %0.2f</p>", $employee["name"], $employee["department"], $net_income);
//}


//$person = [
//    "name" => "Dave",
//    "department" => "ICT",
//    "salary" => 56000.0,
//    "tax" => 4200.0,
//];
//
//
//foreach ( $person as $key => $value) {
//    echo "<p>" . $key . "=> " . $value . "</p>";
//}

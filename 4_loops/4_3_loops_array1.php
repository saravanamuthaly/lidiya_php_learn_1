<?php

$colors = ["red", "blue", "yellow", "magenta", "pink"];

var_dump($colors);
//var_dump(count($colors));

/*
 * while loop
 */
$index = 0;
//while ($index < count($colors)){
//    var_dump($colors[$index]);
//    $index++;
//}

/*
 * for loop
 */
//for ($i = 0; $i < count($colors); $i++){
//    var_dump($colors[$i]);
//}

/*
 * for-each loop
 */

$colors[] = "black";
$colors[] = "green";

$j = 0;
foreach ($colors as $color){
    var_dump($j-- . " => " . $color);

}

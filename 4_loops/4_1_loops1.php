<?php

$index = 0;

var_dump(7 > 4);
var_dump(7 < 4);
var_dump(7 <= 6);
var_dump(7 >= 6);

var_dump(false);
var_dump(true);
var_dump(!true); // not
var_dump(true && true); // and
var_dump(4 < 8 && 3 > 9);

var_dump(true || false || false || true); // or

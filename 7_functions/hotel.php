<?php

$foods = [
    [
        "item" => "omelette",
        "price" => 50.0,
        "count" => 8,
    ],
    [
        "item" => "tea",
        "price" => 30,
        "count" => 15,
    ],
    [
        "item" => "samosa",
        "price" => 25.0,
        "count" => 25,
    ],
];

function get_total( float $price, int $items )
{
    return $price * $items;
}

?>

<!doctype html>
<html lang="en">
<head>
    <title>Document</title>
</head>
<body>

<table border="1">
    <thead>
    <tr>
        <th>Item</th>
        <th>Price</th>
        <th>Count</th>
        <th>Total Amount</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ( $foods as $food ): ?>
        <tr>
            <td><?= $food[ 'item' ] ?></td>
            <td><?= $food[ 'price' ] ?></td>
            <td><?= $food[ 'count' ] ?></td>
            <td><?= get_total($food['price'], $food['count']) ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

</body>
</html>

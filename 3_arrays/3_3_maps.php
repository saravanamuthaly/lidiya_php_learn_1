<?php

//$book_title = "The Lord of the Ring";
//$book_author = "J.R.R Tolkien";
//$book_category = "Classic Fantasy";

$books = [ "The Lord of The Ring", "Sapiens", "1984" ]; // [2,1,0]
$authors = [ "J.R.R Tolkien", "Youval Harari", "George Orwell" ]; // [2,0,1]
$categories = [ "Classic Fantasy", "Non-Fiction", "Dystopian Fiction" ];


//sort( $books );
//sort( $authors );
//
//var_dump( $authors );

$selected_book = 0;

$book1 = [
    "title" => "The lord of the ring",
    "author" => "J.R.R",
    "category" => "Fantasy",
];


$book2 = [
    "title" => "Sapiens",
    "author" => "Harrari",
    "category" => "Non-fiction",
];

//$book_collection = [$book1, $book2];
$book_collection = [
    [
        "title" => "The lord of the ring",
        "author" => "J.R.R",
        "category" => "Fantasy",
    ],
    [
        "title" => "Sapiens",
        "author" => "Harrari",
        "category" => "Non-fiction",
    ],
];

var_dump( $book_collection);
?>

<h1><?= $book_collection[1]["title"] ?></h1>

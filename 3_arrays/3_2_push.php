<?php

$color = "green";

$colors = [ "red", $color, "blue" ];


var_dump( $colors );

/**
 * old method
 */

array_push( $colors, "black" );
array_push( $colors, "pink", "yellow", "orange" );

var_dump( $colors );


/**
 * new method
 */

$colors[] = "maroon";

var_dump( $colors );

/**
 * get the length of the array (size)
 */
var_dump(count($colors));

$colors[3] = "white";

var_dump($colors);

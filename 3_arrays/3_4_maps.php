<?php

$student = [ "name" => "Dave", "grade" => 9 ];

var_dump( $student );

var_dump( $student[ "grade" ] );
var_dump( $student[ "name" ] );

$student[ "name" ] = "Gowri";

var_dump( $student );

$student[ "age" ] = 16;

var_dump( $student );


$employee1 = [
    "name" => "Dave",
    "department" => "ICT",
    "salary" => 56000.0,
    "address" => [
        "house" => "#23",
        "street" => "Bar road",
        "city" => "Batticaloa",
    ],
];

$employee2 = [
    "name" => "Kumar",
    "department" => "Accounting",
    "salary" => 56000.0,
    "address" => [
        "house" => "#23",
        "street" => "Bar road",
        "city" => "Batticaloa",
    ],
];

$emplyees = [$employee1, $employee2];

var_dump($emplyees);

var_dump($emplyees[1]["name"]);

//var_dump($employee1);
//var_dump($employee1["address"]["city"]);
